//
//  SnapShotViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 15/09/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SnapShotViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var topView : UIView?
    @IBOutlet var Navigationview : UIView!
    @IBOutlet weak var btn : UIButton?
    
    @IBOutlet weak var homeBtn : UIImageView?
    @IBOutlet weak var notificationBtn : UIImageView?
    @IBOutlet weak var snapchatBtn : UIImageView?
    
    var appConstants : AppConstants = AppConstants()
    
     @IBOutlet weak var tableView : UITableView?
    
    let arrayList : NSMutableArray = ["Meal Period Revenue","Sales","Top Products","Top Products Profit Wise","Revenue Per Top Category","Top Orders/Sales","Top Customers Revenue","Inventory","Discounts","Employees Overview","Payment Type","Tax Type"]
    
    let subTitleArrayList : NSMutableArray = ["BREAKFAST, LUNCH AND DINNER","COMPARE TWO PERIODS OR COMPARE MERCHANT LOCATIONS","OVER TWO PERIODS SALES WISE OR COMPARE MERCHANT LOCATIONS","COMPARE PERIOD OR COMPARE MERCHANT LOCATION","ORDER OVER & ORDER TYPE PRODUCTS","INCLUDE REPEAT PURCHASE","PROFIT OR CATEGORY WISE","PRODUCT WISE","PROFIT WISE PRODUCTS","","CASH,MASTER CARD,VISA CARD ETC.","PURCHASE VAT,SALES VAT ETC."]
    
    
    override func viewWillAppear(_ animated: Bool) {
        homeBtn?.image = UIImage(named: "ic_home")
        snapchatBtn?.image = UIImage(named: "ic_snapshot_active")
        notificationBtn?.image = UIImage(named: "ic_notification")
        Navigationview.backgroundColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        
        applyPlainShadow(view: topView!)
    }
    
    //MARK : TO add Shadow to view
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 6)
        layer.shadowOpacity = 0.12
        layer.shadowRadius = 2
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
     @IBAction func homeButtonClicked(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 12
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! SnapShotTableCell
        cell.titleLabel?.text = arrayList[indexPath.row] as? String
        cell.titleLabel?.textColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        cell.subTitleLabel?.text = subTitleArrayList[indexPath.row] as? String
        cell.viewButton?.addTarget(self, action: #selector(viewDetails), for: .touchUpInside)
        cell.viewButton?.tag = indexPath.row
       // cell.viewButton?.addTarget(self, action:#selector(viewDetails()), for:.touchUpInside)
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        }
    
    func viewDetails(sender : UIButton)
    {
        
        let buttonTag = sender.tag
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let CompareViewController = storyboard.instantiateViewController(withIdentifier: "CompareViewController") as! CompareViewController
        CompareViewController.titleString = arrayList.object(at: buttonTag) as! NSString
        self.present(CompareViewController, animated:false, completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
