//
//  ViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 03/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

 var appConstants: AppConstants = AppConstants()

class ViewController: UIViewController,apiManagerDelegate {
    
   @IBOutlet weak var SignInButton: UIButton?
   @IBOutlet weak var LoginView : UIView?
    @IBOutlet weak var MobileView : UIView?
    @IBOutlet weak var PasswordView : UIView?
    @IBOutlet weak var SignInView : UIView?
    @IBOutlet weak var ForgotPasswordButton : UIButton?
    @IBOutlet weak var checkbox_image : UIImageView?
    @IBOutlet weak var checkbox_btn : UIButton?
     @IBOutlet weak var userName_TextField : UITextField?
     @IBOutlet weak var password_TextField : UITextField?
    
    var userName_string : NSString = ""
    var password_string : NSString = ""
    
    var apiManager : ApiManager = ApiManager()
    var appConstants : AppConstants = AppConstants()
    
    
    override func viewWillAppear(_ animated: Bool) {
       //  LoginView.backgroundColor = UIColor().HexToColor(appConstants.backgroudColor)
        LoginView?.backgroundColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        LoginView?.layer.cornerRadius = 5.0
        MobileView?.layer.cornerRadius = 10.0
        PasswordView?.layer.cornerRadius = 10.0
        SignInView?.layer.cornerRadius = 20.0
        SignInButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        SignInButton?.layer.cornerRadius = 20.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
    }
    
  @IBAction func SuccessFullLogin()
    {
       //CheckFieldsForInput()
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
    
    func CheckFieldsForInput()
    {
        
        if(userName_TextField?.text != "")&&(password_TextField?.text != "")
        {
            userName_string = (userName_TextField?.text)! as NSString
             password_string = (password_TextField?.text)! as NSString
             LoginApiCall()
        }
        else
        {
        print("please enter username and password")
        }
        //To Do : Call webservice for sign In
    }

    func LoginApiCall()
    {
        print("username : \(userName_string)")
        print("password : \(password_string)")
        self.apiManager.ApiLoginCall (action:"login",userName:userName_string as String,password:password_string as String )
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {  let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let dict : NSDictionary = responseDictionary.value(forKey: "userDetails") as! NSDictionary
                print("dict : \(dict)")
                let userId : NSString = dict.value(forKey:  "usr_id") as! NSString
                let merchantId : NSString = dict.value(forKey: "merchant_id") as! NSString
                print(userId,merchantId)
                appConstants.defaults.set(userId, forKey: "userId")
                appConstants.defaults.set(merchantId, forKey: "merchantId")
            
                let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                self.present(SWRevealViewController, animated:false, completion:nil)
            }
                    }
    }
    
    //MARK: Radio button click actions
    
    var checkboxBool = false
    
    @IBAction func checkBoxButtonClicked(sender: AnyObject) {
        
        if !checkboxBool {
          
            checkbox_image?.image = UIImage(named: "ic_checkbox_selected")
            checkboxBool = true
                   }
        else {
            checkbox_image?.image = UIImage(named : "ic_checkbox_unselected")
            checkboxBool = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
