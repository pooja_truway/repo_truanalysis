//
//  MenuViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 28/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit    

class MenuViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
     @IBOutlet weak var tableView : UITableView?
    
    var menuListText = ["Sales","Top Products","Contomers Review","Inventory","Open Report","Employees Overview","Discounts","Tax Type"]
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300.0)
        
        
       // self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    // MARK:  TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuListText.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      return 50.0
    }
    //Cell for row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! MenuTableViewCell!
        cell?.CellTitleLabel.text = menuListText[indexPath.row]
        return cell!
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        print(indexPath.row)
        
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
