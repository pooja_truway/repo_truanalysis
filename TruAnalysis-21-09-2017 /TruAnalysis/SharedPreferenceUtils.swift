//
//  SharedPreferenceUtils.swift
//  TruAnalysis
//
//  Created by Pooja on 03/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SharedPreferenceUtils: NSObject {
    
    var strSample = NSString()
    static let sharedInstance:SharedPreferenceUtils = {
        let instance = SharedPreferenceUtils ()
        return instance
    } ()
    
    // MARK: Init
    override init() {
        print("My Class Initialized")
        // initialized with variable or property
        strSample = "My String"
    }

}
