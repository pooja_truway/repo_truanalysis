//
//  MenuTableViewCell.swift
//  TruAnalysis
//
//  Created by Pooja on 28/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
   // @IBOutlet weak var CellImage: UIImageView!
    @IBOutlet weak var CellTitleLabel: UILabel!
  //  @IBOutlet weak var CellDropDownImage: UIImageView!
    
    @IBOutlet var toggleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
}
}
