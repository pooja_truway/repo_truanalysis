//
//  SalesViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 29/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SalesViewController: UIViewController {
    
    @IBOutlet weak var btn : UIButton?
    @IBOutlet weak var TopView : UIView?
    @IBOutlet weak var topBarView : UIView?
    @IBOutlet weak var titleLabel : UILabel?
    
    @IBOutlet weak var moduleView : UIView?
    @IBOutlet weak var viewDetailBtn : UIButton?
    @IBOutlet weak var titleLbl : UILabel?
    @IBOutlet weak var subtitleLbl : UILabel?
    
    var titleString : NSString = ""
    
    override func viewWillAppear(_ animated: Bool) {
        
         topBarView?.backgroundColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        
        viewDetailBtn?.backgroundColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        
        titleLabel?.text = titleString as String
       // let revealViewController = self.revealViewController()
       // btn?.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
      //  view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        applyPlainShadow(view: TopView!)
        applyPlainShadow(view: moduleView!)
        // Do any additional setup after loading the view.
    }
    
    //MARK : TO add Shadow to view
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 6)
        layer.shadowOpacity = 0.12
        layer.shadowRadius = 2
    }

    //MARK: Back Button Action
    @IBAction func BackClicked(sender : UIButton)
    {
       self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
