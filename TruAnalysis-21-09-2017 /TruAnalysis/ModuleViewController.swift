//
//  ModuleViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 15/09/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class ModuleViewController: UIViewController {
    
    @IBOutlet weak var moduleView : UIView?
    @IBOutlet weak var viewDetailBtn : UIButton?
    @IBOutlet weak var titleLbl : UILabel?
    @IBOutlet weak var subtitleLbl : UILabel?
    @IBOutlet weak var week1Lbl : UILabel?
    @IBOutlet weak var week2Lbl : UILabel?
    @IBOutlet weak var week3Lbl : UILabel?
    @IBOutlet weak var week4Lbl : UILabel?
    
    @IBOutlet weak var week1Price : UILabel?
    @IBOutlet weak var week2Price : UILabel?
    @IBOutlet weak var week3Price : UILabel?
    @IBOutlet weak var week4Price : UILabel?
    
    
    override func viewWillAppear(_ animated: Bool) {
        applyPlainShadow(view: moduleView!)
    }
    
    //MARK : TO add Shadow to view
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 4, height: 6)
        layer.shadowOpacity = 0.19
        layer.shadowRadius = 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
