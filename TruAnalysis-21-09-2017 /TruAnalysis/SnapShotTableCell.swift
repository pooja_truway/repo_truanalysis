//
//  SnapShotTableCell.swift
//  TruAnalysis
//
//  Created by Pooja on 18/09/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SnapShotTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var subTitleLabel : UILabel?
    @IBOutlet weak var week1Value : UILabel?
    @IBOutlet weak var week2Value : UILabel?
    @IBOutlet weak var week3Value : UILabel?
    @IBOutlet weak var week4Value : UILabel?
    @IBOutlet weak var viewButton : UIButton?
    @IBOutlet weak var titleView : UIView?

    override func awakeFromNib() {
        super.awakeFromNib()
        viewButton?.layer.cornerRadius = 15.0
        viewButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        titleView?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.backgroudColorDark).withAlphaComponent(1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
