//
//  AppConstants.swift
//  TruAnalysis
//
//  Created by Pooja on 03/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import Foundation
import UIKit

class AppConstants {
    
    // MARK: - Application Constants
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    let defaults = UserDefaults.standard

    
    //UI Constants
    let CornerRadius: CGFloat = 5.0
    let backgroudColor = "#ba6c1f"
    let backgroudColorDark = "#6e3007"
    
    let color1 = "#1F77B4"
    let color2 = "#FF7F0E"
    let color3 = "#2CA02C"
    let color4 = "#D62728"
    let color5 = "#9467BD"
    let color6 = "#8C564B"
    let color7 = "#E377C2"
    let color8 = "#7F7F7F"
    let color9 = "#BCBD22"
    let color10 = "#17BECF"
    
    //MARK: Base URL
    let BaseURL = "http://truanalysis.solutions/webservices"
    
    //MARK: - To show Alert View
    func showAlert(title: String , message: String, controller: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
}
