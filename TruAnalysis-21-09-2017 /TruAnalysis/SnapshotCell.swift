//
//  SnapshotCell.swift
//  TruAnalysis
//
//  Created by Pooja on 28/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SnapshotCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl : UILabel?
    @IBOutlet weak var imageview : UIImageView?
    @IBOutlet weak var btn : UIButton?
    
}
