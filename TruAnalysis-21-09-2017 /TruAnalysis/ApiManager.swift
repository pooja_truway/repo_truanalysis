//
//  ApiManager.swift
//  TruwayMPS
//
//  Created by Pooja on 29/05/17.
//  Copyright © 2017 Truway India. All rights reserved.


import UIKit
import Alamofire
import SwiftyJSON

protocol apiManagerDelegate:NSObjectProtocol {
    
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>)
    func APIFailureResponse(_ msgError: String)
}

extension apiManagerDelegate {
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        print ("hello apimanager \(response)")
    }
    
    func APIFailureResponse(_ msgError: String){
        // leaving this empty
    }
}

class ApiManager: NSObject,apiManagerDelegate {
    
    fileprivate let API_STATUS = "error_status"
    var delegate :apiManagerDelegate?
    var appConstants: AppConstants = AppConstants()
    
    // MARK: HUD Utility
    func showHud()  {
        // HUD.flash(.rotatingImage(UIImage(named: "progress")), delay: 2.0)
    }
    func showlongHud()  {
        // HUD.flash(.rotatingImage(UIImage(named: "progress")), delay: 5.0)
    }
    
    func hideHud()  {
        
    }
    //MARK: Get App Status
    func ApiLoginCall (action:String,userName:String,password: String){
        
        print("parameters : \(userName),\(password)")
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"userName":userName,"passwd":password], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    
    
    //MARK: Get App Status
    func ApiPieChartDataCall (action:String,merchantId:String,limit: String){
        
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"merchantId":merchantId,"limit":limit], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
 }
