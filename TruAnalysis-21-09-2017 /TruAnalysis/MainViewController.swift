//
//  MainViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 04/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import Charts

weak var barChartView: BarChartView?
var months: [String]!


class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        
        setChart(dataPoints: months, values: unitsSold)
        // Do any additional setup after loading the view.
    }
    
    
    func setChart(dataPoints: [String], values: [Double]) {
        barChartView?.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
//        for i in 0..<dataPoints.count {
//           // let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
//            let dataEntry = BarChartDataEntry(x: values[i], y: Double(i))
//            dataEntries.append(dataEntry)
//        }
//        
//      //  let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Units Sold")
//        let chartDataSet1 = BarChartDataSet(values: dataEntries, label: "Units Sold")
//        let chartData = BarChartData(xVals: months, dataSet: chartDataSet1)
//        
//        let chartData1 = BarChartData(
//        barChartView?.data = chartData1
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
