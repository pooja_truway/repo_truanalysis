//
//  HomeViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 25/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import Charts

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate ,UIScrollViewDelegate,apiManagerDelegate{
    
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var lineChartView2: LineChartView!
     @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var pieChartView1: PieChartView!
    @IBOutlet weak var pieChartView2: PieChartView!
    @IBOutlet weak var barChartView : BarChartView!
    @IBOutlet weak var scrollview : UIScrollView?
    
    let months = ["Mon", "Tues", "Wed", "Thur", "Fri","Sat","Sun"]
    let unitsSold = [0.75,1.55,2.0,3.5, 4.8, 2.5, 3.0]
    let unitsSold1 = ["0","50","100","150","200","250","300"]
    var pieValues = [Double]()
    var pieStringArray = [String]()
    var selectedStringValue : NSString = ""
    var selectedStringIndex : NSString = ""
    var yVals2 =  [ChartDataEntry]()
    
    var fromCell : NSString = ""
    
    var pieValuesFromServer = [Double]()
    var pieLabelFromServer : NSMutableArray = []

    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var topView : UIView?
    @IBOutlet var Navigationview : UIView!
    @IBOutlet weak var btn : UIButton?
    
    @IBOutlet weak var homeBtn : UIImageView?
    @IBOutlet weak var notificationBtn : UIImageView?
    @IBOutlet weak var snapchatBtn : UIImageView?
    
    @IBOutlet weak var salesview : UIView?
    @IBOutlet weak var productsview : UIView?
    @IBOutlet weak var inventoryview : UIView?
    @IBOutlet weak var customersview : UIView?
    
    @IBOutlet weak var saleslabel : UILabel?
    @IBOutlet weak var productslabel : UILabel?
    @IBOutlet weak var inventorylabel : UILabel?
    @IBOutlet weak var customerslabel : UILabel?
    
    @IBOutlet weak var collectionView : UICollectionView?
     @IBOutlet weak var collectionViewBarChart : UICollectionView?
     let reuseIdentifier = "cell"
     var PieChartTitleArray : NSMutableArray =  []
    
    var selectedIndex = Int ()
    
    var barChartStringArray = [String]()
    var barChartValues = [Double]()
    
    
    
    var colors : NSMutableArray = [UIColor().HexToColor(hexString: appConstants.color1),UIColor().HexToColor(hexString: appConstants.color2),UIColor().HexToColor(hexString: appConstants.color3),UIColor().HexToColor(hexString: appConstants.color4),UIColor().HexToColor(hexString: appConstants.color5),UIColor().HexToColor(hexString: appConstants.color6),UIColor().HexToColor(hexString: appConstants.color7),UIColor().HexToColor(hexString: appConstants.color8),UIColor().HexToColor(hexString: appConstants.color9),UIColor().HexToColor(hexString: appConstants.color10)]
    
    var colors1 : NSMutableArray = [UIColor().HexToColor(hexString: appConstants.color1),UIColor().HexToColor(hexString: appConstants.color2),UIColor().HexToColor(hexString: appConstants.color3),UIColor().HexToColor(hexString: appConstants.color4),UIColor().HexToColor(hexString: appConstants.color5),UIColor().HexToColor(hexString: appConstants.color6),UIColor().HexToColor(hexString: appConstants.color7),UIColor().HexToColor(hexString: appConstants.color8),UIColor().HexToColor(hexString: appConstants.color9),UIColor().HexToColor(hexString: appConstants.color10)]
    
    var selectedBtn = false
    var unselectedBtn = false
    
    var apiManager : ApiManager = ApiManager()
    
    override func viewWillAppear(_ animated: Bool) {
        Navigationview.backgroundColor = UIColor().HexToColor(hexString: appConstants.backgroudColor).withAlphaComponent(1)
        salesview?.isHidden = false
        productsview?.isHidden = false
        inventoryview?.isHidden = false
        customersview?.isHidden = false
        
        saleslabel?.isHidden = false
        productslabel?.isHidden = false
        inventorylabel?.isHidden = false
        customerslabel?.isHidden = false
        
        collectionView?.isHidden = false
        collectionViewBarChart?.isHidden = false
        self.scrollview?.delegate = self
       
        self.scrollview?.contentSize = CGSize(width: self.view.frame.size.width, height: 2400)
        
        //ADD Gesture to Menu Button
        let revealViewController = self.revealViewController()
        btn?.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //Add Gesture to view 1
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer.delegate = self
    
        salesview?.addGestureRecognizer(gestureRecognizer)
        
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer1.delegate = self
        
    productsview?.addGestureRecognizer(gestureRecognizer1)
        
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer2.delegate = self
        inventoryview?.addGestureRecognizer(gestureRecognizer2)
        
        let gestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer3.delegate = self
        customersview?.addGestureRecognizer(gestureRecognizer3)
        
        salesview?.tag = 1
        productsview?.tag = 2
        inventoryview?.tag = 3
        customersview?.tag = 4
        
        collectionView?.tag = 1
        collectionViewBarChart?.tag = 2
        
        getPieChartData()
        }
    
    //MARK: Api to get pie chart data
    func getPieChartData()
    {
        self.apiManager.ApiPieChartDataCall (action:"topViewProducts",merchantId:"M42ZHS6PR2MFG" as String,limit:"10" as String )
    }
    
    //MARK: Tap Gesture for view 1
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SalesViewController = storyboard.instantiateViewController(withIdentifier: "SalesViewController") as! SalesViewController
        let tag = gestureRecognizer.view?.tag
        if(tag == 1)
        {
            SalesViewController.titleString = "SALES"
        }
        else if (tag == 2)
        {
          SalesViewController.titleString = "TOP PRODUCTS"
        }
        else if (tag == 3)
        {
            SalesViewController.titleString = "INVENTORY"
        }
        
        else if (tag == 4)
        {
            SalesViewController.titleString = "TOP CUSTOMER REVENUE"
        }
        
        self.navigationController?.pushViewController(SalesViewController, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self

        setChartData(data: (months,unitsSold))
        setChartData2(data: (months,unitsSold))
        applyPlainShadow(view: topView!)
        applyPlainShadow(view: collectionView!)
        self.navigationController?.navigationBar.isHidden = true
        //Define Layout here
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        //Get device width
        let width = UIScreen.main.bounds.width
        
        //set section inset as per your requirement.
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        
        //set cell item size here
        layout.itemSize = CGSize(width: 108, height: 30)
        //set Minimum spacing between 2 itemsr
        
        layout.minimumInteritemSpacing = 2
        //set minimum vertical line spacing here between two lines in collectionview
        layout.minimumLineSpacing = 2
        
        //apply defined layout to collectionview
        collectionView!.collectionViewLayout = layout
        
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 270
            print("HOME")
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        barChartStringArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"]
        barChartValues = [24.0, 22.0, 20.0, 18.0, 16.0, 14.0, 12.0, 10.0, 8.0, 6.0, 4.0, 2.0]
         setChart(dataPoints: barChartStringArray, values: barChartValues)
        // Do any additional setup after loading the view.
    }
}
   //MARK : TO add Shadow to view
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 6)
        layer.shadowOpacity = 0.12
        layer.shadowRadius = 2
    }
    //MARK: bar chart data set
    func setChart(dataPoints: [String], values: [Double]) {
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x : values[i], y : Double(i))
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Income")
        let chartData = BarChartData(dataSet: chartDataSet)
        chartDataSet.drawValuesEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        chartDataSet.colors = [UIColor().HexToColor(hexString: appConstants.color1),UIColor().HexToColor(hexString: appConstants.color2),UIColor().HexToColor(hexString: appConstants.color3),UIColor().HexToColor(hexString: appConstants.color4),UIColor().HexToColor(hexString: appConstants.color5),UIColor().HexToColor(hexString: appConstants.color6),UIColor().HexToColor(hexString: appConstants.color7),UIColor().HexToColor(hexString: appConstants.color8),UIColor().HexToColor(hexString: appConstants.color9),UIColor().HexToColor(hexString: appConstants.color10)]
        
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.drawLabelsEnabled = true
        barChartView.xAxis.labelWidth = 3
        barChartView.xAxis.axisLineWidth = 1
        barChartView.xAxis.labelTextColor = UIColor.white
        barChartView.xAxis.axisLineColor = UIColor.white
        barChartView.xAxis.labelPosition = .bottom
        
        barChartView.rightAxis.enabled = false
        barChartView.drawBarShadowEnabled = false
        
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.enabled = true
        barChartView.leftAxis.axisLineColor = UIColor.darkGray
        barChartView.leftAxis.labelTextColor = UIColor.darkGray
        barChartView.leftAxis.labelCount = 4
        barChartView.leftAxis.axisLineWidth = 1
        barChartView.leftAxis.drawZeroLineEnabled = true
        
        barChartView.legend.enabled = false
        barChartView.data = chartData
    }
    
    //MARK: Button Actions Of Top View
    //MARK: Radio button click actions
    
    var homeBtnBool = false
    var snapchatBtnBool = false
    var notificationBtnBool = false
    @IBAction func homeButtonClicked(sender: AnyObject) {
        
        if !homeBtnBool {
            homeBtn?.image = UIImage(named: "ic_home_active")
            snapchatBtn?.image = UIImage(named: "ic_snapshot")
            notificationBtn?.image = UIImage(named: "ic_notification")
           
            print("Check box 1 selected")
            snapchatBtnBool = false
            notificationBtnBool = false
            homeBtnBool = true
            salesview?.isHidden = false
            productsview?.isHidden = false
            inventoryview?.isHidden = false
            customersview?.isHidden = false
            
            saleslabel?.isHidden = false
            productslabel?.isHidden = false
            inventorylabel?.isHidden = false
            customerslabel?.isHidden = false
            collectionView?.isHidden = false
           
        } else {
            
                 }
    }
    
    @IBAction func snapchatButtonClicked(sender: AnyObject) {
        if !snapchatBtnBool {
            
            homeBtn?.image = UIImage(named: "ic_home")
            snapchatBtn?.image = UIImage(named: "ic_snapshot_active")
            notificationBtn?.image = UIImage(named: "ic_notification")
            
            print("Check box 2 selected")
            snapchatBtnBool = true
            notificationBtnBool = false
            homeBtnBool = false
            
            collectionView?.isHidden = false
            
            salesview?.isHidden = true
            productsview?.isHidden = true
            inventoryview?.isHidden = true
            customersview?.isHidden = true
            
            saleslabel?.isHidden = true
            productslabel?.isHidden = true
            inventorylabel?.isHidden = true
            customerslabel?.isHidden = true
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SnapShotViewController = storyboard.instantiateViewController(withIdentifier: "SnapShotViewController") as! SnapShotViewController
            self.present(SnapShotViewController, animated:false, completion:nil)
        }
        else {
           // snapchatBtn?.image = UIImage(named : "ic_snapchat_active")
           // snapchatBtnBool = false
        }
    }
    
    @IBAction func notificationButtonClicked(sender: AnyObject) {
        
        if !notificationBtnBool {
            homeBtn?.image = UIImage(named: "ic_home")
            snapchatBtn?.image = UIImage(named: "ic_snapshot")
            notificationBtn?.image = UIImage(named: "ic_notification_active")
            
            print("Check box 3 selected")
            snapchatBtnBool = false
            notificationBtnBool = true
            homeBtnBool = false
            
            salesview?.isHidden = true
            productsview?.isHidden = true
            inventoryview?.isHidden = true
            customersview?.isHidden = true
            
            saleslabel?.isHidden = true
            productslabel?.isHidden = true
            inventorylabel?.isHidden = true
            customerslabel?.isHidden = true
            collectionView?.isHidden = false

        } else {
          //  notificationBtn?.image = UIImage(named: "ic_notification")
          // notificationBtnBool = false
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == collectionView)
        {
          return PieChartTitleArray.count
        }
        else{
        return PieChartTitleArray.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if(collectionView == collectionView)
        {
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! SnapshotCell
        
        cell = cellA
        if selectedIndex == indexPath.row
        {
            cellA.imageview?.backgroundColor = self.colors[indexPath.item] as? UIColor
          //cellA.lbl?.textColor = UIColor.lightGray
            cellA.tag = 1
        }
        else
        {
          
        }
      
        cellA.btn?.tag = indexPath.item
        cellA.lbl?.text = self.PieChartTitleArray[indexPath.item] as? String
        cellA.btn?.titleLabel?.text = self.PieChartTitleArray[indexPath.item] as? String
        cellA.btn?.isUserInteractionEnabled = false
        cellA.imageview?.backgroundColor = self.colors[indexPath.item] as? UIColor
      //  return cell
        }
        else{
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! SnapshotCell
            
            cell = cellA
            if selectedIndex == indexPath.row
            {
                cellA.imageview?.backgroundColor = self.colors[indexPath.item] as? UIColor
                //cellA.lbl?.textColor = UIColor.lightGray
                cellA.tag = 1
            }
            else
            {
                
            }
            
            cellA.btn?.tag = indexPath.item
            cellA.lbl?.text = self.PieChartTitleArray[indexPath.item] as? String
            cellA.btn?.titleLabel?.text = self.PieChartTitleArray[indexPath.item] as? String
            cellA.btn?.isUserInteractionEnabled = false
            cellA.imageview?.backgroundColor = self.colors[indexPath.item] as? UIColor
           // return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! SnapshotCell
        print(unselectedBtn)
        
      
        if let cell = collectionView.cellForItem(at: indexPath) {
            
            if(cell.contentView.alpha == 1)
            {
            
            cell.contentView.alpha = 0.2
            unselectedBtn = true
            let str =  cellA.lbl?.text
            
            print("title \(str)")
            selectedStringValue = PieChartTitleArray.object(at: indexPath.item) as! NSString
            
            print("selectedStringValue :\(selectedStringValue)")
            
            yVals2.removeAll()
            fromCell = "Selected"
            setPieChartData(data: (pieStringArray,pieValues,colors as! [UIColor]))
            }
            
            else{
                 cell.contentView.alpha = 1.0
                yVals2.removeAll()
               fromCell = "UnSelected"
            selectedStringIndex = PieChartTitleArray.object(at: indexPath.item) as! NSString
                
            print("selectedStringIndex :\(selectedStringIndex)")
            let pieNewValue = pieValuesFromServer[indexPath.item]
            print("done : \(pieNewValue)")
                
            pieStringArray.append(selectedStringIndex as String)
            pieValues.append(pieNewValue)
            let colorToAdd = colors1[indexPath.item]
           colors.add(colorToAdd)
                                
            print("color array : \(colors.count)")
                                
            setPieChartData(data: (pieStringArray,pieValues,colors as! [UIColor]))
            }
        }
       
        else{
            
        }
    }
    
    
    
    //MARK: btn sender action
    func cellButtonTapped(sender: UIButton)
    {
        //remove from pie chart
        if !selectedBtn {
        print("btn action : \(sender.tag)")
        sender.isUserInteractionEnabled = true
        sender.alpha = 0.5
        let str =  sender.titleLabel?.text
        print("title \(str)")
        selectedStringValue = PieChartTitleArray.object(at: sender.tag) as! NSString
        yVals2.removeAll()
        setPieChartData(data: (pieStringArray,pieValues,colors as! [UIColor]))
       //selectedBtn = true
        }
         else{                                                 //add data to pie chart
            sender.isUserInteractionEnabled = true
              sender.alpha = 1.0
            selectedStringValue = "empty"
            print("else case : \(sender.tag)")
        let pieValueNew = pieValuesFromServer[sender.tag]
            pieValues.append(pieValueNew)
            print("new value array : \(pieValues)")
            print("new value : \(pieValueNew)")
            print("colors array \(colors)")
            //selectedBtn = false
        }
    }
    
    func setPieChartData(data: ([String],[Double],[UIColor]))
    {
        for i in 0 ..< data.0.count {
        yVals2.append(ChartDataEntry(x: Double(i), y: data.1[i]))
            if(fromCell == "Selected")
            { print("123 \(data.0[i])")
            if(data.0[i] == selectedStringValue as String)
            {
              yVals2.remove(at: i)
                print("matched \(data.0[i])")
                pieValues.remove(at: i)
                colors.removeObject(at: i)
                pieStringArray.remove(at: i)
                
                print("method pie values\(pieValues.count)")
                print("method colors.count\(colors.count)")
                print("method pieStringArray\(pieStringArray.count)")
                selectedStringValue = ""
                let formatter = NumberFormatter()
                formatter.numberStyle = .percent
                formatter.maximumFractionDigits = 1
                formatter.multiplier = 1.0
                
                let pieChartDataSet = PieChartDataSet(values: yVals2, label: "pie chart")
                let pieChartData = PieChartData(dataSet: pieChartDataSet)
                pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
                pieChartView.data = pieChartData
                
                pieChartView1.data = pieChartData //
                pieChartView1.drawHoleEnabled = true
                 pieChartView1.holeRadiusPercent = 0.5
                pieChartView1.centerText = "View Top Orders"
                
                pieChartView2.data = pieChartData //
                pieChartView2.drawHoleEnabled = true
                pieChartView2.holeRadiusPercent = 0.5
                pieChartView2.centerText = "View Top Customers"

                pieChartDataSet.colors = colors as! [NSUIColor]
                pieChartDataSet.sliceSpace = 2.5
            }
                
        }
                else if(fromCell == "UnSelected")
            {
                
              //  yVals2.append(<#T##newElement: ChartDataEntry##ChartDataEntry#>)
                print("data 123 \(data.0[i])")
                
                print("method pie values\(pieValues.count)")
                print("method colors.count\(colors.count)")
                print("method pieStringArray\(pieStringArray.count)")
              
               // pieValues.remove(at: i)
               // colors.removeObject(at: i)
               // pieStringArray.remove(at: i)
            }
                
            else{
                if(selectedStringValue == "empty")
                {
                 let AddValue = pieValuesFromServer[i]
                pieValues.insert(AddValue, at: i)
                print("pie added values : \(pieValues)")
                }
                    
           
                print(" not matched \(data.0[i])")
                let formatter = NumberFormatter()
                formatter.numberStyle = .percent
                formatter.maximumFractionDigits = 1
                formatter.multiplier = 1.0
                
                let pieChartDataSet = PieChartDataSet(values: yVals2, label: "pie chart")
                let pieChartData = PieChartData(dataSet: pieChartDataSet)
                
                pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
                pieChartView.data = pieChartData
                 pieChartView1.data = pieChartData //pooja
                pieChartView1.drawHoleEnabled = true
                pieChartView1.holeRadiusPercent = 0.5
                 pieChartView1.centerText = "View Top Orders"
                
                pieChartView2.data = pieChartData //
                pieChartView2.drawHoleEnabled = true
                pieChartView2.holeRadiusPercent = 0.5
                pieChartView2.centerText = "View Top Customers"
                pieChartDataSet.colors = colors as! [NSUIColor]
                pieChartDataSet.sliceSpace = 2.5
            }
            
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .percent
            formatter.maximumFractionDigits = 1
            formatter.multiplier = 1.0
            
            let pieChartDataSet = PieChartDataSet(values: yVals2, label: "pie chart")
            let pieChartData = PieChartData(dataSet: pieChartDataSet)
            pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
            pieChartView.data = pieChartData
            pieChartView1.data = pieChartData
            pieChartView1.drawHoleEnabled = true//pooja
             pieChartView1.holeRadiusPercent = 0.5
             pieChartView1.centerText = "View Top Orders"
            
            pieChartView2.data = pieChartData //
            pieChartView2.drawHoleEnabled = true
            pieChartView2.holeRadiusPercent = 0.5
            pieChartView2.centerText = "View Top Customers"
            
            pieChartDataSet.colors = colors as! [NSUIColor]
            pieChartDataSet.sliceSpace = 2.5

            print("data. 1 : \(ChartDataEntry(x: data.1[i], y: Double(i)))")
        }
            pieChartView.animate(xAxisDuration: 1.4)
    }
    
    func setChartData(data: ([String], [Double])) {
        var yVals1 = [ChartDataEntry]()
        for i in 0 ..< data.0.count {
            yVals1.append(ChartDataEntry(x: Double(i), y: data.1[i]))
            print("data. 1 : \(ChartDataEntry(x: data.1[i], y: Double(i)))")
        }
        
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "Income")
        set1.axisDependency = .left
        set1.setColor(UIColor(red:255/255, green:255/255, blue:255/255, alpha:1.000))
        set1.lineWidth = 1.0
        set1.fillAlpha = 1
        set1.fillColor = UIColor(red:51/255, green:181/255, blue:235/255, alpha:1)
        set1.highlightColor = UIColor.clear
        set1.drawIconsEnabled = true
        set1.drawValuesEnabled = true
        set1.drawCirclesEnabled = true
        set1.drawFilledEnabled = true
        set1.circleColors = [UIColor.white]
        
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        
        let data: LineChartData = LineChartData(dataSets: dataSets)
        data.setValueTextColor(UIColor.white)
        
        let legend = lineChartView.legend
        legend.enabled = true
        
        let xAxis = lineChartView.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelTextColor = UIColor.white
        
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:months)
        
        let rightAxis = lineChartView.rightAxis
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawGridLinesEnabled = false
        
        let leftAxis = lineChartView.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.gridColor = UIColor.white.withAlphaComponent(0.1)
        leftAxis.gridLineWidth = 2
        leftAxis.labelTextColor = UIColor.white
        
        lineChartView.leftAxis.valueFormatter = IndexAxisValueFormatter(values: unitsSold1)
        
        self.lineChartView.pinchZoomEnabled = false
        self.lineChartView.isUserInteractionEnabled = true
        self.lineChartView.drawMarkers = false
        self.lineChartView.data = data
        self.lineChartView.marker = "hi" as? IMarker
        
        self.lineChartView.chartDescription?.text = "Live Updates"
        self.lineChartView.chartDescription?.textColor = UIColor.white
    }
    
    //MARK: Line Chart For Comparision
    func setChartData2(data: ([String], [Double])) {
        var yVals1 = [ChartDataEntry]()
        for i in 0 ..< data.0.count {
            yVals1.append(ChartDataEntry(x: Double(i), y: data.1[i]))
            print("data. 1 : \(ChartDataEntry(x: data.1[i], y: Double(i)))")
        }
        
        let months1 = ["Mon", "Tues", "Wed", "Thur", "Fri","Sat","Sun"]
        let unitsSold2 = [0.55,1.05,2.20,3.3, 2.1, 2.20, 1.50]

        
        var yVals2 = [ChartDataEntry]()
        for i in 0 ..< months1.count {
            yVals2.append(ChartDataEntry(x: Double(i), y: unitsSold2[i]))
            print("data. 1 : \(ChartDataEntry(x: unitsSold2[i], y: Double(i)))")
        }
        
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "Aug-2017")
        set1.axisDependency = .left
        set1.setColor(UIColor(red:0/255, green:50/255, blue:200/255, alpha:1.000))
        set1.lineWidth = 1.0
        set1.fillAlpha = 1
        set1.fillColor = UIColor.clear
        set1.highlightColor = UIColor.clear
        set1.drawIconsEnabled = true
        set1.drawValuesEnabled = true
        set1.drawCirclesEnabled = true
        set1.drawFilledEnabled = true
        set1.circleColors = [UIColor.blue]
        set1.circleRadius = 4.0
        
        let set2: LineChartDataSet = LineChartDataSet(values: yVals2, label: "Sept-2017")
        set2.axisDependency = .left
        set2.setColor(UIColor(red:226/255, green:210/255, blue:7/255, alpha:1.000))
        set2.lineWidth = 1.0
        set2.fillAlpha = 1
        set2.fillColor = UIColor.clear
        set2.highlightColor = UIColor.clear
        set2.drawIconsEnabled = true
        set2.drawValuesEnabled = true
        set2.drawCirclesEnabled = true
        set2.drawFilledEnabled = true
        set2.circleColors = [UIColor.yellow]
        set2.circleRadius = 4.0
        
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
       dataSets.append(set1)
        dataSets.append(set2)
        
        let data: LineChartData = LineChartData(dataSets: dataSets)
        data.setValueTextColor(UIColor.clear)
        
        let legend = lineChartView.legend
        legend.enabled = true
        
        let xAxis = lineChartView.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelTextColor = UIColor.white
        
        lineChartView2.xAxis.valueFormatter = IndexAxisValueFormatter(values:months)
        
        let rightAxis = lineChartView.rightAxis
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawGridLinesEnabled = false
        
        let leftAxis = lineChartView.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.gridColor = UIColor.white.withAlphaComponent(0.1)
        leftAxis.gridLineWidth = 2
        leftAxis.labelTextColor = UIColor.white
        
        lineChartView2.leftAxis.valueFormatter = IndexAxisValueFormatter(values: unitsSold1)
        
        self.lineChartView2.pinchZoomEnabled = false
        self.lineChartView2.isUserInteractionEnabled = true
        self.lineChartView2.drawMarkers = false
        self.lineChartView2.data = data
        self.lineChartView2.marker = "hi" as? IMarker
        
        self.lineChartView2.chartDescription?.text = "Comparison"
        self.lineChartView2.chartDescription?.textColor = UIColor.black
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseArray : NSArray = value as! NSArray
             print("dict : \(responseArray)")
             print("dict count : \(responseArray.count)")
            for str in responseArray
            {
                print("str: \(str)")
                let responseStr : NSString = (str as?
                    NSString)!
                let tagArray = responseStr.components(separatedBy: "=")
                let tagStr = tagArray.dropLast() //get tags of pie chart
                let tagStr1 : NSString = tagStr.first! as NSString
                print("testing \(tagStr1)")
                PieChartTitleArray.insert(tagStr1 as String, at: 0)
                let words = responseStr.components(separatedBy: "(")
                let str1 = words.dropFirst()
                print("str1 : \(str1)")
                let str2 : NSString = str1.first! as NSString
                print("str2 \(str2)")
                
                print("words: \(words)")
                 let words1 = str2.components(separatedBy: ")")
                print("words1: \(words1)")
                let str3 = words1.dropLast()
                 print("final string str3 \(str3)")
                
                 let str4 : NSString = str3.first! as NSString
               
                let str5 : NSString = str4.replacingOccurrences(of: ",", with: "") as NSString //replace occurance of ,
                
                let pieChartValues: Double = (str5 as NSString).doubleValue
                
                pieValues.insert(pieChartValues, at: 0)
                pieValuesFromServer.insert(pieChartValues, at: 0)
                print("new values 11: \(pieValues)")
        }
            pieValues = pieValues.reversed()
            PieChartTitleArray = PieChartTitleArray.reversed() as! NSMutableArray
            
            //to store the data
            pieValuesFromServer = pieValues
            pieLabelFromServer = PieChartTitleArray
            
            pieStringArray = PieChartTitleArray as! [String]
            print("values of label : \(PieChartTitleArray)")
            collectionView?.reloadData()
             setPieChartData(data: (pieStringArray,pieValues,colors as! [UIColor]))
    }
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
