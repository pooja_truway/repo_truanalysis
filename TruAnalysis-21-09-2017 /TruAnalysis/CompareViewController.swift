//
//  CompareViewController.swift
//  TruAnalysis
//
//  Created by Pooja on 18/09/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import Charts

class CompareViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,ChartViewDelegate{
    
    @IBOutlet weak var TopView : UIView?
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var startDateField : UITextField?
    @IBOutlet weak var endDateField : UITextField?
    @IBOutlet weak var Ok_Btn : UIButton?
    @IBOutlet weak var summaryLabel : UILabel?
    @IBOutlet weak var summaryView : UIView?
    
    @IBOutlet weak var scrollview : UIScrollView?
    
    @IBOutlet weak var barChartView : BarChartView!
    @IBOutlet weak var barChartView1 : BarChartView!
    var barChartStringArray = [String]()
    var barChartValues = [Double]()
    
    
    var barChartStringArray1 = [String]()
    var barChartValues1 = [Double]()
    
    var referenceStringArray = [String]()
    var referenceValues = [Double]()

    var colors : NSMutableArray = [UIColor().HexToColor(hexString: appConstants.color3),UIColor().HexToColor(hexString: appConstants.color6),UIColor().HexToColor(hexString: appConstants.color8),UIColor().HexToColor(hexString: appConstants.color9),UIColor().HexToColor(hexString: appConstants.color1),UIColor().HexToColor(hexString: appConstants.color2),UIColor().HexToColor(hexString: appConstants.color4),UIColor().HexToColor(hexString: appConstants.color5),UIColor().HexToColor(hexString: appConstants.color7),UIColor().HexToColor(hexString: appConstants.color10)]
    
    var titleString : NSString = ""
    @IBOutlet weak var collectionView : UICollectionView?
    @IBOutlet weak var collectionView2 : UICollectionView?
    let reuseIdentifier = "cell"
    
    var selectedIndex = Int ()
    @IBOutlet weak var heading1Label : UILabel?
    @IBOutlet weak var heading2Label : UILabel?
    var selectedBtn = false
    
    override func viewWillAppear(_ animated: Bool) {
        titleLabel?.text = titleString as String
        Ok_Btn?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.backgroudColorDark).withAlphaComponent(1)
        Ok_Btn?.layer.cornerRadius = 15.0
        summaryLabel?.isHidden = true
        summaryView?.isHidden = true
        barChartView.isHidden = true
        collectionView?.isHidden = true
        self.scrollview?.delegate = self
        heading1Label?.isHidden = true
        
        self.scrollview?.contentSize = CGSize(width: self.view.frame.size.width, height: 400)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        applyPlainShadow(view: TopView!)
        barChartStringArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"]
        barChartValues = [24.0, 22.0, 20.0, 18.0, 16.0, 14.0, 12.0, 10.0, 8.0, 6.0, 4.0, 2.0]
        
        barChartStringArray1 = ["Jan", "Feb", "Mar", "Apr"]
        barChartValues1 = [24.0, 22.0, 20.0, 18.0 ]
        
        referenceStringArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"]
        referenceValues = [24.0, 22.0, 20.0, 18.0, 16.0, 14.0, 12.0, 10.0, 8.0, 6.0, 4.0, 2.0]
        
        barChartView.isHidden = true
        barChartView1.isHidden = true

    // Do any additional setup after loading the view.
    }
    //MARK : TO add Shadow to view
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 6)
        layer.shadowOpacity = 0.12
        layer.shadowRadius = 2
    }
    
    //MARK: TextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print(textField.tag)
        switch (textField.tag)
        {
        
        case 0:
            print("start period")
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.tag = 0
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
            break
        case 1:
            print("end period")
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.tag = 1
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
            break
                default:
            print("default")
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }

    //MARK: Date Picker
    func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_GB") as Locale! // using Great Britain for 24 hr format
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd/MM/YYYY"
        if(sender.tag == 0)
        {
        startDateField?.text = dateFormatter.string(from: sender.date)
        }
        else{
        endDateField?.text = dateFormatter.string(from: sender.date)
        }
    }
    
    //MARK: bar chart data set
    func setChart(dataPoints: [String], values: [Double]) {
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x : values[i], y : Double(i))
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: titleString as String)
        let chartData = BarChartData(dataSet: chartDataSet)
        chartDataSet.drawValuesEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        chartDataSet.colors = [UIColor().HexToColor(hexString: appConstants.color1),UIColor().HexToColor(hexString: appConstants.color2),UIColor().HexToColor(hexString: appConstants.color3),UIColor().HexToColor(hexString: appConstants.color4),UIColor().HexToColor(hexString: appConstants.color5),UIColor().HexToColor(hexString: appConstants.color6),UIColor().HexToColor(hexString: appConstants.color7),UIColor().HexToColor(hexString: appConstants.color8),UIColor().HexToColor(hexString: appConstants.color9),UIColor().HexToColor(hexString: appConstants.color10)]
        
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.drawLabelsEnabled = true
        barChartView.xAxis.labelWidth = 3
        barChartView.xAxis.axisLineWidth = 1
        barChartView.xAxis.labelTextColor = UIColor.white
        barChartView.xAxis.axisLineColor = UIColor.white
        barChartView.xAxis.labelPosition = .bottom
        
        barChartView.rightAxis.enabled = false
        barChartView.drawBarShadowEnabled = false
        
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.enabled = true
        barChartView.leftAxis.axisLineColor = UIColor.darkGray
        barChartView.leftAxis.labelTextColor = UIColor.darkGray
        barChartView.leftAxis.labelCount = 4
        barChartView.leftAxis.axisLineWidth = 1
        barChartView.leftAxis.drawZeroLineEnabled = true
        
     //  barChartView.getHighlightByTouchPoint(<#T##pt: CGPoint##CGPoint#>)
        
        
        
        barChartView.legend.enabled = false
        barChartView.chartDescription?.enabled = false
        barChartView.data = chartData
    }
    
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("Bar selected")
    }

    
    //MARK: bar chart 1 data set
    func setChart1(dataPoints: [String], values: [Double]) {
        barChartView1.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x : values[i], y : Double(i))
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: titleString as String)
        let chartData = BarChartData(dataSet: chartDataSet)
        chartDataSet.drawValuesEnabled = false
        barChartView1.drawGridBackgroundEnabled = false
        chartDataSet.colors = [UIColor().HexToColor(hexString: appConstants.color3),UIColor().HexToColor(hexString: appConstants.color6),UIColor().HexToColor(hexString: appConstants.color8),UIColor().HexToColor(hexString: appConstants.color9),UIColor().HexToColor(hexString: appConstants.color1),UIColor().HexToColor(hexString: appConstants.color2),UIColor().HexToColor(hexString: appConstants.color4),UIColor().HexToColor(hexString: appConstants.color5),UIColor().HexToColor(hexString: appConstants.color7),UIColor().HexToColor(hexString: appConstants.color10)]
        
        barChartView1.xAxis.drawGridLinesEnabled = false
        barChartView1.xAxis.drawLabelsEnabled = true
        barChartView1.xAxis.labelWidth = 3
        barChartView1.xAxis.axisLineWidth = 1
        barChartView1.xAxis.labelTextColor = UIColor.white
        barChartView1.xAxis.axisLineColor = UIColor.white
        barChartView1.xAxis.labelPosition = .bottom
        
        barChartView1.rightAxis.enabled = false
        barChartView1.drawBarShadowEnabled = false
        
        barChartView1.leftAxis.drawGridLinesEnabled = false
        barChartView1.leftAxis.enabled = true
        barChartView1.leftAxis.axisLineColor = UIColor.darkGray
        barChartView1.leftAxis.labelTextColor = UIColor.darkGray
        barChartView1.leftAxis.labelCount = 4
        barChartView1.leftAxis.axisLineWidth = 1
        barChartView1.leftAxis.drawZeroLineEnabled = true
        
        barChartView1.legend.enabled = false
        barChartView1.data = chartData
    }

    @IBAction func ok_ButtonAction (sender : UIButton)
    {
        summaryLabel?.isHidden = false
        summaryView?.isHidden = false
        barChartView.isHidden = false
        barChartView1.isHidden = false
        collectionView?.isHidden = false
        heading1Label?.isHidden = false

         setChart(dataPoints: barChartStringArray, values: barChartValues)
         setChart1(dataPoints: barChartStringArray1, values: barChartValues1)
        
        self.scrollview?.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)

    }

    //MARK: Back Button Action
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SnapShotViewController = storyboard.instantiateViewController(withIdentifier: "SnapShotViewController") as! SnapShotViewController
        self.present(SnapShotViewController, animated:false, completion:nil)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return 10
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! SnapshotCell
            
            cell = cellA
            if selectedIndex == indexPath.row
            {
               cellA.imageview?.backgroundColor = colors[indexPath.item] as? UIColor
                //cellA.lbl?.textColor = UIColor.lightGray
                cellA.tag = 1
            }
            else
            {
            }
            cellA.btn?.tag = indexPath.item
            cellA.lbl?.text = self.barChartStringArray[indexPath.item] as? String
            cellA.btn?.titleLabel?.text = self.barChartStringArray[indexPath.item] as? String
            cellA.btn?.isUserInteractionEnabled = false
            cellA.imageview?.backgroundColor = self.colors[indexPath.item] as? UIColor
               return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! SnapshotCell
      //  print(unselectedBtn)
        
        
        if let cell = collectionView.cellForItem(at: indexPath) {
            
            if(cell.contentView.alpha == 1)
            {
                cell.contentView.alpha = 0.2
              //  unselectedBtn = true
                let str =  cellA.lbl?.text
                
                   print("array count before selection : \(barChartStringArray.count)")
                print("title \(str)")
                
                let abc : NSMutableArray = [barChartStringArray.remove(at: indexPath.item)]
                print("array count : \(barChartStringArray.count)")
                print("abc array : \(abc)")
                
                let xyz : NSMutableArray = [barChartValues.remove(at: indexPath.item)]
                
            setChart(dataPoints: barChartStringArray, values: barChartValues)
                           }
                
            else{
                cell.contentView.alpha = 1.0
                let abc = referenceStringArray[indexPath.item]
                barChartStringArray.append(abc)
                
                let xyz = referenceValues[indexPath.item]
                barChartValues.append(xyz)
                
                 print("array count after adding : \(barChartStringArray.count)")
                
                 setChart(dataPoints: barChartStringArray, values: barChartValues)
                print("color array : \(colors.count)")
                
            }
        }
            
        else{
            
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
